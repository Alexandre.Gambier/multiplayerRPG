const express = require('express')
const app = express()
const server = require('http').Server(app)
const io = require('socket.io')(server)
// const ent = require('ent');
let room = 0,
  ready = 0,
  green = false,
  red = false,
  blue = false,
  orange = false
const Player = require('./class/Player')

app.use('/css', express.static(__dirname + '/css'))
app.use('/js', express.static(__dirname + '/js'))
app.use('/bower_components', express.static(__dirname + '/bower_components'))
app.use('/assets', express.static(__dirname + '/assets'))

app.get('/', function (req, res) {
  res.sendFile(__dirname + '/index.html')
})

server.listen(process.env.PORT || 8084, function () {
  console.log('Listening on ' + server.address().port)
})

io.sockets.on('connection', function (socket) {
  socket.playerId = randomInt(0, 4)
  socket.roomId = randomInt(0, 10000)
  socket.muchDamageId = null

  socket.join(socket.roomId)

  socket.emit('roomId', socket.roomId)

  socket.on('sendId', function () {
    const id = socket.playerId++ % 4
    socket.to(socket.roomId).emit('id', id)
    socket.emit('id', id)
  })

  socket.on('id', function (id) {
    socket.playerId = id
  })

  socket.on('connected', function (data) {
    socket.roomId = data.roomId
    socket.join(socket.roomId)
    socket.to(socket.roomId).emit('connected')
  })

  socket.on('controller', function (pseudo) {
    socket.player = new Player({
      username: pseudo,
      intel: randomInt(10, 50),
      strength: randomInt(10, 50),
      agility: randomInt(10, 50),
    })
    socket.emit('player', socket.player)
    socket.emit('isConnected', getUsernames(socket.roomId))
    socket.to(socket.roomId).emit('isConnected', pseudo)
  })

  socket.on('forcedLaunch', function () {
    console.log('forced launch', socket)
    socket.to(socket.roomId).emit('forcedLaunch')
  })

  socket.on('notController', function (pseudo) {
    socket.boss = new Player({
      username: 'Boss',
      intel: randomInt(10, 100),
      strength: randomInt(10, 100),
      agility: randomInt(10, 100),
    })
    socket.emit('player', socket.boss)
  })

  socket.on('attack', function () {
    socket.action = 'attack'
    socket
      .to(socket.roomId)
      .emit('wantToAttack', { player: socket.player, id: socket.playerId })
  })

  socket.on('update', function (player) {
    socket.player.life = player.life
    socket.player.mana = player.mana
  })

  socket.on('launch', function () {
    socket.to(socket.roomId).emit('launch')
  })

  socket.on('win', function () {
    socket.to(socket.roomId).emit('win')
    socket.emit('win')
  })

  socket.on('guard', function () {
    socket.action = 'guard'
    socket
      .to(socket.roomId)
      .emit('wantToGuard', { player: socket.player, id: socket.playerId })
  })

  socket.on('heal', function (who) {
    socket.action = 'heal'
    socket.who = who
    socket
      .to(socket.roomId)
      .emit('wantToHeal', { player: socket.player, id: socket.playerId })
  })

  socket.on('comp', function (id) {
    socket.action = 'comp'
    socket.compId = id
    socket.to(socket.roomId).emit('wantToUse', {
      player: socket.player,
      id: socket.playerId,
      compId: id,
    })
  })

  socket.on('low', function (id) {
    socket.to(socket.roomId).emit('low', socket.playerId)
  })

  socket.on('unlow', function (id) {
    socket.to(socket.roomId).emit('unlow', socket.playerId)
  })

  socket.on('dead', function (id) {
    socket.player.life = 0
    socket.to(socket.roomId).emit('dead', socket.playerId)
    socket.emit('dead', socket.playerId)
  })

  socket.on('doTurn', function () {
    let muchDamage = 0
    const tmp = getAllPlayers(socket.roomId)
    let i = 0
    const interv = setInterval(function () {
      if (tmp[i].action == 'attack') {
        let damage = tmp[i].player.getDegats()
        muchDamage = muchDamage > damage ? muchDamage : damage
        if (muchDamage == damage) socket.muchDamageId = tmp[i].playerId
        socket.boss.life -= damage
        socket.emit('damage', {
          id: tmp[i].playerId,
          boss: socket.boss,
          username: tmp[i].player.username,
          damage: damage,
        })
        tmp[i].action = null
      } else if (tmp[i].action == 'heal') {
        tmp[i].action = null
        let p = null
        for (var j = 0; j < tmp.length; j++) {
          if (tmp[i].who == tmp[j].player.username) p = tmp[j]
        }
        if (p === null) return
        console.log('who :', tmp[i].who)
        var heal = null
        if (tmp[i].player.mana >= 10 && p.player.life > 0) {
          tmp[i].player.mana -= 10
          heal = tmp[i].player.getHeal()
          p.player.life += heal
          if (p.player.life > p.player.maxLife) p.player.life = p.player.maxLife
          socket
            .to(socket.roomId)
            .emit('heal', { id: p.playerId, player: p.player, heal: heal })
          socket.emit('heal', { id: p.playerId, player: p.player, heal: heal })
          socket
            .to(socket.roomId)
            .emit('mana', { id: tmp[i].playerId, player: tmp[i].player })
        } else if (tmp[i].player.mana >= 50) {
          tmp[i].player.mana -= 50
          heal = tmp[i].player.getHeal()
          p.player.life += heal
          console.log('revive :', heal)
          if (p.player.life > p.player.maxLife) p.player.life = p.player.maxLife
          socket
            .to(socket.roomId)
            .emit('heal', { id: p.playerId, player: p.player })
          socket.emit('heal', { id: p.playerId, player: p.player, heal: heal })
          socket.emit('revive', {
            id: p.playerId,
            player: p.player,
            heal: heal,
          })
          socket
            .to(socket.roomId)
            .emit('mana', { id: tmp[i].playerId, player: tmp[i].player })
        }
      }
      i++
      if (i >= tmp.length) clearInterval(interv)
    }, 200)
    setTimeout(function () {
      if (
        socket.muchDamageId != null &&
        socket.boss.life > 0 &&
        ((socket.boss.mana < 10 &&
          socket.boss.life > (25 / 100) * socket.boss.maxLife) ||
          randomInt(0, 100) > 10)
      ) {
        console.log(socket.muchDamageId)
        getOneByPlayerId(socket.roomId, socket.muchDamageId, function (user) {
          user.player.life -=
            damage - user.action == 'guard' ? randomInt(1, 5) : 0
          console.log(socket.boss.agility, user.player.agility)
          if (
            randomInt(0, 1000) > 800 &&
            socket.boss.mana > 100 &&
            socket.boss.life < (socket.boss.maxLife * 30) / 100
          ) {
            socket.boss.mana -= 100
            var damage = socket.boss.getMagicDamage()
            socket.to(socket.roomId).emit('magicHurt', damage)
            socket.emit('magicHurt')
          } else if (
            socket.boss.agility + randomInt(0, 100) <
            user.player.agility
          ) {
            socket.emit('evade', { id: socket.muchDamageId })
          } else {
            let damage = socket.boss.getDegats()
            socket.to(socket.roomId).emit('hurt', {
              id: socket.muchDamageId,
              damage: damage,
              username: user.player.username,
            })
            socket.emit('hurt', {
              id: socket.muchDamageId,
              damage: damage,
              username: user.player.username,
            })
            if (user.player.life <= 0) {
              socket.muchDamageId = null
              socket.to(socket.roomId).emit('dead', socket.muchDamageId)
              socket.emit('dead', socket.muchDamageId)
            }
          }
        })
      } else if (socket.muchDamageId != null && socket.boss.life > 0) {
        socket.boss.life += socket.boss.getHeal()
        socket.boss.mana -= 10
        if (socket.boss.life > socket.boss.maxLife)
          socket.boss.life = socket.boss.maxLife
        socket.emit('healBoss', socket.boss)
      }
      setTimeout(function () {
        console.log('timeout')
        socket.to(socket.roomId).emit('endTurn')
        socket.emit('endTurn')
      }, 1000)
    }, 1000)
  })

  socket.on('loose', function () {
    socket.to(socket.roomId).emit('loose')
    socket.emit('loose')
  })

  socket.on('needHeal', function () {
    socket.to(socket.roomId).emit('needHeal', socket.playerId)
  })

  socket.on('disconnect', function () {
    socket.to(socket.roomId).emit('disconnected', {
      id: socket.playerId,
      username: socket.player ? socket.player.username : null,
    })
  })
})

function getAllPlayers(room) {
  console.log('get All')
  var players = []
  //console.log(io.sockets.connected);
  io.sockets.sockets.entries().forEach(function ([socketID, playerSocket]) {
    var player =
      playerSocket.roomId == room && playerSocket.player ? playerSocket : null
    if (player) players.push(player)
  })
  //console.log(players);
  return players
}

function getUsernames(room) {
  const players = []

  console.log(io.sockets)
  io.sockets.sockets.entries().forEach(function ([socketID, playerSocket]) {
    const player =
      playerSocket.roomId == room && playerSocket.player
        ? playerSocket.player.username
        : null
    if (player) players.push(player)
  })
  return players
}

function getOneByUsername(room, username, callback) {
  io.sockets.sockets.entries().forEach(function ([socketID, playerSocket]) {
    var player =
      playerSocket.roomId == room &&
      playerSocket.player &&
      playerSocket.player.username == username
        ? playerSocket
        : null
    if (player) return callback(player)
  })
}

function getOneByPlayerId(room, playerId, callback) {
  io.sockets.sockets.entries().forEach(function ([socketID, playerSocket]) {
    var player =
      playerSocket.roomId == room &&
      playerSocket.player &&
      playerSocket.playerId == playerId
        ? playerSocket
        : null
    if (player) return callback(player)
  })
}

function randomInt(low, high) {
  return Math.floor(Math.random() * (high - low) + low)
}
