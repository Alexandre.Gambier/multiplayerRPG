var Player = function(data){
	this.username = data.username;
	this.maxLife = data.strength*20 + data.agility*10 +100;
	this.life = this.maxLife;
	this.lvl = 1;
	this.maxMana = data.intel * 20+100;
	this.mana = this.maxMana;
	this.agility = data.agility;
	this.strength = data.strength;
	this.intel = data.intel;
	this.exp = 0;
	this.gold = 0;
	this.competencesPts = 0;
}

Player.prototype.getDegats = function () {
	return this.strength + randomInt(0,10);
};

Player.prototype.addStrength = function(){
	this.competencesPts--;
	this.strength++;
	this.maxLife = this.strength*20 + this.agility*10 +100;
}

Player.prototype.addIntel = function(){
	this.intel++;
	this.competencesPts--;
	this.maxMana = this.intel*20+100;
}

Player.prototype.addAgility = function(){
	this.agility++;
	this.competencesPts--;
	this.maxLife = this.strength*20 + this.agility*10 +100;
}

Player.prototype.getHeal = function () {
	return this.intel + randomInt(0,10);
};

Player.prototype.getMagicDamage = function () {
	return this.intel + randomInt(100,300);
};

module.exports = Player;

function randomInt (low, high) {
    return Math.floor(Math.random() * (high - low) + low);
}
