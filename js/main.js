var config = {
  type: typeof Phaser === 'undefined' ? null : Phaser.WEBGL,
  width: window.innerWidth,
  height: window.innerHeight,
  antialias: true,
  roundPixels: true,
  scene: [fight],
}

function launchGame() {
  console.log('launch game')
  $('#qrcode')
    .children()
    .each(function (i, obj) {
      $(obj).remove()
    })
  $('.explain').remove()
  new Phaser.Game(config)
  socket.emit('launch')
}
