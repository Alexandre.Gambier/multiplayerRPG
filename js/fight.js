var fight = {
  bgf: null,
  bgw: null,
  actors: [],
  boss: null,
  start: false,
  ambiance: null,
  victory: null,
  blow: null,
  heal: null,
  gameover: null,
  heals: [],
  bossDamage: null,
  bossHeal: null,
  bossMagic: null,
  playerMessage: [],
  playerHeal: [],
  playerDammage: [],
  hit: [[], []],
  preload: function () {
    for (var i = 1; i < 5; i++) {
      this.load.spritesheet('char' + i, 'assets/Actor1_' + i + '.png', {
        frameWidth: 64,
        frameHeight: 64,
      })
    }
    for (var i = 1; i < 3; i++) {
      this.load.spritesheet('hit' + i, 'assets/Hit' + i + '.png', {
        frameWidth: 192,
        frameHeight: 192,
      })
    }
    for (var i = 1; i < 4; i++) {
      this.load.image('wall' + i, 'assets/wall' + i + '.png')
      this.load.image('floor' + i, 'assets/floor' + i + '.png')
    }

    for (var i = 1; i < 4; i++) {
      this.load.image('boss' + i, 'assets/boss' + i + '.png')
    }

    this.load.spritesheet('healAnim', 'assets/Recovery.png', {
      frameWidth: 192,
      frameHeight: 192,
    })
    this.load.spritesheet('dark', 'assets/Darkness.png', {
      frameWidth: 192,
      frameHeight: 192,
    })
    this.load.audio('music', ['assets/Battle.m4a'])
    this.load.audio('gameover', ['assets/Gameover.m4a'])
    this.load.audio('victory', ['assets/Victory2.m4a'])
    this.load.audio('blow', ['assets/Blow.m4a'])
    this.load.audio('heal', ['assets/Heal.m4a'])
    this.load.audio('darkness', ['assets/Darkness.m4a'])

    var progressBar = this.add.graphics()
    var progressBox = this.add.graphics()
    progressBox.fillStyle(0x222222, 0.8)
    progressBox.fillRect(10, 10, 320, 50)

    this.load.on('progress', function (value) {
      //console.log(value);
      progressBar.clear()
      progressBar.fillStyle(0xffffff, 1)
      progressBar.fillRect(10, 10, 300 * value, 30)
    })
    this.load.on('complete', function () {
      progressBar.destroy()
      progressBox.destroy()
    })
  },
  create: function () {
    var fw = randomInt(1, 3)
    console.log(fw)
    fight.bgf = this.add.image(0, 0, 'floor' + fw).setOrigin(0)
    fight.bgw = this.add.image(0, 0, 'wall' + fw).setOrigin(0)
    fight.bgf.setScale(config.width / fight.bgf.width)
    fight.bgw.setScale(config.width / fight.bgw.width)
    fight.ambiance = this.sound.add('music')
    fight.victory = this.sound.add('victory')
    fight.bossMagicSound = this.sound.add('darkness')
    fight.blow = this.sound.add('blow')
    fight.heal = this.sound.add('heal')
    fight.gameover = this.sound.add('gameover')
    var loopMarker = {
      name: 'loop',
      start: 0,
      duration: 77,
      config: {
        loop: true,
      },
    }

    fight.ambiance.addMarker(loopMarker)
    fight.ambiance.setVolume(0.0002)
    fight.ambiance.play('loop', {
      delay: 0,
    })
    for (var i = 1; i < socket.count + 1; i++) {
      fight.actors[socket.players[i - 1]] = {}
      fight.actors[socket.players[i - 1]].idle = true
      fight.actors[socket.players[i - 1]].chooseHit = false
      fight.actors[socket.players[i - 1]].cast = false
      fight.actors[socket.players[i - 1]].lowHP = false
      fight.actors[socket.players[i - 1]].alive = true
      fight.actors[socket.players[i - 1]].sprite = this.add.sprite(
        config.width - config.width / 3 + 45 * i,
        config.height / 2.5 + 60 * i,
        'char' + (socket.players[i - 1] + 1)
      )
      fight.heals[socket.players[i - 1]] = this.add.sprite(
        fight.actors[socket.players[i - 1]].sprite.x,
        fight.actors[socket.players[i - 1]].sprite.y,
        'healAnim'
      )
      fight.heals[socket.players[i - 1]].visible = false
      fight.actors[socket.players[i - 1]].sprite.setScale(
        (64 * fight.actors[socket.players[i - 1]].sprite.width) / config.width
      )
      this.anims.create({
        key: 'idle' + (socket.players[i - 1] + 1),
        frames: this.anims.generateFrameNumbers(
          'char' + (socket.players[i - 1] + 1),
          { start: 0, end: 2 }
        ),
        frameRate: 10,
        yoyo: true,
        repeat: -1,
      })
      this.anims.create({
        key: 'hit' + (socket.players[i - 1] + 1),
        frames: this.anims.generateFrameNumbers(
          'char' + (socket.players[i - 1] + 1),
          { start: 3, end: 5 }
        ),
        frameRate: 10,
      })
      this.anims.create({
        key: 'evade1' + (socket.players[i - 1] + 1),
        frames: this.anims.generateFrameNumbers(
          'char' + (socket.players[i - 1] + 1),
          { start: 6, end: 8 }
        ),
        frameRate: 10,
      })
      this.anims.create({
        key: 'chooseHit' + (socket.players[i - 1] + 1),
        frames: this.anims.generateFrameNumbers(
          'char' + (socket.players[i - 1] + 1),
          { start: 9, end: 11 }
        ),
        frameRate: 10,
        yoyo: true,
        repeat: -1,
      })
      this.anims.create({
        key: 'drawWeapon' + (socket.players[i - 1] + 1),
        frames: this.anims.generateFrameNumbers(
          'char' + (socket.players[i - 1] + 1),
          { start: 12, end: 14 }
        ),
        frameRate: 10,
      })
      this.anims.create({
        key: 'victory' + (socket.players[i - 1] + 1),
        frames: this.anims.generateFrameNumbers(
          'char' + (socket.players[i - 1] + 1),
          { start: 15, end: 17 }
        ),
        frameRate: 10,
        yoyo: true,
        repeat: -1,
      })
      this.anims.create({
        key: 'cast' + (socket.players[i - 1] + 1),
        frames: this.anims.generateFrameNumbers(
          'char' + (socket.players[i - 1] + 1),
          { start: 18, end: 20 }
        ),
        frameRate: 10,
        yoyo: true,
        repeat: -1,
      })
      this.anims.create({
        key: 'castFire' + (socket.players[i - 1] + 1),
        frames: this.anims.generateFrameNumbers(
          'char' + (socket.players[i - 1] + 1),
          { start: 21, end: 23 }
        ),
        frameRate: 10,
      })
      this.anims.create({
        key: 'magicHurt' + (socket.players[i - 1] + 1),
        frames: this.anims.generateFrameNumbers(
          'char' + (socket.players[i - 1] + 1),
          { start: 24, end: 26 }
        ),
        frameRate: 10,
      })
      this.anims.create({
        key: 'guard' + (socket.players[i - 1] + 1),
        frames: this.anims.generateFrameNumbers(
          'char' + (socket.players[i - 1] + 1),
          { start: 27, end: 29 }
        ),
        frameRate: 10,
      })
      this.anims.create({
        key: 'provoc1' + (socket.players[i - 1] + 1),
        frames: this.anims.generateFrameNumbers(
          'char' + (socket.players[i - 1] + 1),
          { start: 30, end: 32 }
        ),
        frameRate: 10,
      })
      this.anims.create({
        key: 'physicHurt' + (socket.players[i - 1] + 1),
        frames: this.anims.generateFrameNumbers(
          'char' + (socket.players[i - 1] + 1),
          { start: 36, end: 38 }
        ),
        frameRate: 10,
      })
      this.anims.create({
        key: 'provoc2' + (socket.players[i - 1] + 1),
        frames: this.anims.generateFrameNumbers(
          'char' + (socket.players[i - 1] + 1),
          { start: 39, end: 41 }
        ),
        frameRate: 10,
      })
      this.anims.create({
        key: 'lowHP' + (socket.players[i - 1] + 1),
        frames: this.anims.generateFrameNumbers(
          'char' + (socket.players[i - 1] + 1),
          { start: 42, end: 44 }
        ),
        frameRate: 10,
        yoyo: true,
        repeat: -1,
      })
      this.anims.create({
        key: 'evade2' + (socket.players[i - 1] + 1),
        frames: this.anims.generateFrameNumbers(
          'char' + (socket.players[i - 1] + 1),
          { start: 45, end: 47 }
        ),
        frameRate: 10,
      })
      /*this.anims.create({
                key: ''+i,
                frames: this.anims.generateFrameNumbers('char'+i, { start: 48, end: 50 }),
                frameRate: 10
            });*/
      this.anims.create({
        key: 'dead' + (socket.players[i - 1] + 1),
        frames: this.anims.generateFrameNumbers(
          'char' + (socket.players[i - 1] + 1),
          { start: 51, end: 53 }
        ),
        frameRate: 10,
        yoyo: true,
        repeat: -1,
      })
      fight.actors[socket.players[i - 1]].sprite.anims.play(
        'idle' + (socket.players[i - 1] + 1),
        true
      )
      fight.hit[0][socket.players[i - 1]] = this.add.sprite(
        fight.actors[socket.players[i - 1]].sprite.x +
          fight.actors[socket.players[i - 1]].sprite.width / 2,
        fight.actors[socket.players[i - 1]].sprite.y,
        'hit1'
      )
      fight.hit[0][socket.players[i - 1]].visible = false
      //fight.hit[0][socket.players[i-1]].setScale(2);
      fight.hit[1][socket.players[i - 1]] = this.add.sprite(
        fight.actors[socket.players[i - 1]].sprite.x +
          fight.actors[socket.players[i - 1]].sprite.width / 2,
        fight.actors[socket.players[i - 1]].sprite.y,
        'hit2'
      )
      fight.hit[1][socket.players[i - 1]].visible = false
      //fight.hit[1][socket.players[i-1]].setScale(2);
      fight.playerMessage[socket.players[i - 1]] = this.add.text(
        fight.actors[socket.players[i - 1]].sprite.x -
          fight.actors[socket.players[i - 1]].sprite.width,
        fight.actors[socket.players[i - 1]].sprite.y -
          fight.actors[socket.players[i - 1]].sprite.height *
            fight.actors[socket.players[i - 1]].sprite._scaleY,
        '',
        {
          fontSize: '40px',
          fill: '#fff',
        }
      )
      fight.playerDammage[socket.players[i - 1]] = this.add.text(
        fight.actors[socket.players[i - 1]].sprite.x -
          fight.actors[socket.players[i - 1]].sprite.width,
        fight.actors[socket.players[i - 1]].sprite.y -
          fight.actors[socket.players[i - 1]].sprite.height *
            fight.actors[socket.players[i - 1]].sprite._scaleY,
        '',
        {
          fontSize: '40px',
          fill: '#f00',
        }
      )
      fight.playerHeal[socket.players[i - 1]] = this.add.text(
        fight.actors[socket.players[i - 1]].sprite.x -
          fight.actors[socket.players[i - 1]].sprite.width,
        fight.actors[socket.players[i - 1]].sprite.y -
          fight.actors[socket.players[i - 1]].sprite.height *
            fight.actors[socket.players[i - 1]].sprite._scaleY,
        '',
        {
          fontSize: '40px',
          fill: '#0f0',
        }
      )
    }
    fight.start = true
    fight.boss = this.add
      .image(config.width / 20, config.height / 2, 'boss' + randomInt(1, 3))
      .setOrigin(0, 0.5)
    fight.heals[4] = this.add.sprite(
      fight.boss.x + fight.boss.width / 2,
      fight.boss.y,
      'healAnim'
    )
    fight.heals[4].setScale(3)
    fight.heals[4].visible = false

    fight.bossMagic = this.add.sprite(
      config.width - config.width / 3,
      config.height / 2,
      'dark'
    )
    fight.bossMagic.visible = false
    fight.bossMagic.setScale(5)

    fight.bossDammage = this.add.text(
      fight.boss.x - fight.boss.width,
      fight.boss.y - fight.boss.height * fight.boss._scaleY,
      '',
      {
        fontSize: '40px',
        fill: '#f00',
      }
    )
    fight.bossHeal = this.add.text(
      fight.boss.x - fight.boss.width,
      fight.boss.y - fight.boss.height * fight.boss._scaleY,
      '',
      {
        fontSize: '40px',
        fill: '#0f0',
      }
    )

    fight.hit[0][4] = this.add.sprite(
      fight.boss.x + fight.boss.width / 2,
      fight.boss.y,
      'hit1'
    )
    fight.hit[0][4].visible = false
    fight.hit[0][4].setScale(2)
    this.anims.create({
      key: 'hitAnim0',
      frames: this.anims.generateFrameNumbers('hit1', { start: 0, end: 3 }),
      frameRate: 10,
      hideOnComplete: true,
      showOnStart: true,
    })
    this.anims.create({
      key: 'darkness',
      frames: this.anims.generateFrameNumbers('dark', { start: 0, end: 24 }),
      frameRate: 10,
      hideOnComplete: true,
      showOnStart: true,
    })
    fight.hit[1][4] = this.add.sprite(
      fight.boss.x + fight.boss.width / 2,
      fight.boss.y,
      'hit2'
    )
    fight.hit[1][4].visible = false
    fight.hit[1][4].setScale(2)
    this.anims.create({
      key: 'hitAnim1',
      frames: this.anims.generateFrameNumbers('hit2', { start: 0, end: 2 }),
      frameRate: 10,
      hideOnComplete: true,
      showOnStart: true,
    })
    this.anims.create({
      key: 'heal',
      frames: this.anims.generateFrameNumbers('healAnim', {
        start: 0,
        end: 29,
      }),
      frameRate: 10,
      hideOnComplete: true,
      showOnStart: true,
    })
  },
  doHit: function (id, who) {
    fight.hit[id][who].anims.play('hitAnim' + id, true)
  },
  update: function () {
    if (!fight.start) return
    /*for( var i = 0; i < fight.actors.length ; i++){
            if(!fight.actors[i].alive) fight.actors[i].sprite.anims.play('dead'+(i+1), true);
            else if(fight.actors[i].lowHP) fight.actors[i].sprite.anims.play('lowHP'+(i+1), true);
            else if(fight.actors[i].idle) fight.actors[i].sprite.anims.play('idle'+(i+1), true);
            else if(fight.actors[i].chooseHit) fight.actors[i].sprite.anims.play('chooseHit'+(i+1), true);
            else if(fight.actors[i].cast) fight.actors[i].sprite.anims.play('cast'+(i+1), true);
            else if(fight.actors[i].guard) fight.actors[i].sprite.anims.play('guard'+(i+1), true);
        }*/
  },
}
