var socket = io.connect('')
socket.roomId = null

if (mobilecheck()) {
  $('.explain').html("Entrez votre nom d'utilisateur")
  socket.playerId = null
  socket.roomId = $_GET('roomId')
  if (!socket.roomId) {
    $('body').empty()
    $('body').append(
      $('<label for="roomNumber">').html('Numéro de salle'),
      $('<input id="roomNumber">'),
      $('<br>'),
      $('<br>'),
      $('<button>')
        .html('Connexion')
        .on('click', function () {
          window.location =
            socket.io.uri +
            socket.io.engine.hostname +
            ':' +
            socket.io.engine.port +
            '?roomId=' +
            $('#roomNumber').val()
        })
    )
  }
  socket.on('connect', function () {
    socket.emit('connected', { roomId: socket.roomId })
  })

  socket.on('launch', function () {
    if ($('#force')) $('#force').remove()
  })

  socket.on('magicHurt', function (damage) {
    socket.player.life -= damage
    $('#life').css(
      'width',
      (socket.player.life / socket.player.maxLife) * 100 + '%'
    )
    $('#lifeText').html(socket.player.life + '/' + socket.player.maxLife)
    if (socket.player.life <= 0) {
      socket.emit('dead')
    } else if (socket.player.life < (30 * socket.player.maxLife) / 100) {
      $('#life').css('backgroundColor', 'red')
      socket.emit('low')
    } else if (socket.player.life < (50 * socket.player.maxLife) / 100)
      $('#life').css('backgroundColor', 'orange')
    else $('#life').css('backgroundColor', 'green')
    socket.emit('update', socket.player)
  })

  socket.on('player', function (player) {
    console.log(player)
    $('.explain').remove()
    $('#username').html(player.username)
    $('#life').css('backgroundColor', 'green')
    $('#life').css('width', (player.life / player.maxLife) * 100 + '%')
    $('#mana').css('width', (player.mana / player.maxMana) * 100 + '%')
    $('#lifeText').html(player.life + '/' + player.maxLife)
    $('#manaText').html(player.mana + '/' + player.maxMana)
    $('.stats').css('display', 'flex')
    $('.details').append(
      $('<p>').html('Strength : ' + player.strength),
      $('<p>').html('Agility : ' + player.agility),
      $('<p>').html('Intel : ' + player.intel)
    )
    socket.player = player
  })
  socket.on('endTurn', function () {
    if (socket.player.life <= 0) return
    $('.controls').css('display', 'flex')
    $('#mana').css(
      'width',
      (socket.player.mana / socket.player.maxMana) * 100 + '%'
    )
    $('#manaText').html(socket.player.mana + '/' + socket.player.maxMana)
    if (socket.player.life <= 0) {
      socket.player.life = 0
      socket.emit('dead')
    } else if (socket.player.life < (30 * socket.player.maxLife) / 100) {
      $('#life').css('backgroundColor', 'red')
      socket.emit('low')
    } else if (socket.player.life < (50 * socket.player.maxLife) / 100)
      $('#life').css('backgroundColor', 'orange')
    else $('#life').css('backgroundColor', 'green')
    socket.emit('update', socket.player)
  })
  socket.on('isConnected', function (usernames) {
    if (typeof usernames == 'object')
      for (var i = 0; i < usernames.length; i++) {
        $(
          `<input type="radio" name="who" id="who-${i}" value="${usernames[i]}">`
        )
          .appendTo($('.players'))
          .after(usernames[i])
      }
    else
      $(`<input type="radio" name="who" id="who-0" value="${usernames}">`)
        .appendTo($('.players'))
        .after(usernames)

    $('#who-0').attr('selected', true)
  })

  socket.on('heal', function (data) {
    console.log('h :', data, socket.playerId)
    if (socket.playerId != data.id) return
    console.log(data)
    socket.player = data.player
    $('#life').css(
      'width',
      (socket.player.life / socket.player.maxLife) * 100 + '%'
    )
    $('#lifeText').html(socket.player.life + '/' + socket.player.maxLife)
    if (socket.player.life <= 0) {
      socket.emit('dead')
    } else if (socket.player.life < (30 * socket.player.maxLife) / 100) {
      $('#life').css('backgroundColor', 'red')
      socket.emit('low')
    } else if (socket.player.life < (50 * socket.player.maxLife) / 100) {
      $('#life').css('backgroundColor', 'orange')
      socket.emit('unLow')
    } else $('#life').css('backgroundColor', 'green')
    socket.emit('update', socket.player)
  })

  socket.on('mana', function (data) {
    if (socket.playerId != data.id) return
    socket.player.mana = data.player.mana
    $('#mana').css(
      'width',
      (socket.player.mana / socket.player.maxMana) * 100 + '%'
    )
    $('#manaText').html(socket.player.mana + '/' + socket.player.maxMana)
  })

  socket.on('hurt', function (data) {
    if (socket.playerId != data.id) return
    console.log('hurt ', data)
    socket.player.life -= data.damage
    $('#life').css(
      'width',
      (socket.player.life / socket.player.maxLife) * 100 + '%'
    )
    $('#lifeText').html(socket.player.life + '/' + socket.player.maxLife)
    if (socket.player.life <= 0) {
      socket.emit('dead')
    } else if (socket.player.life < (30 * socket.player.maxLife) / 100) {
      $('#life').css('backgroundColor', 'red')
      socket.emit('low')
    } else if (socket.player.life < (50 * socket.player.maxLife) / 100) {
      $('#life').css('backgroundColor', 'orange')
      socket.emit('unLow')
    } else $('#life').css('backgroundColor', 'green')
    socket.emit('update', socket.player)
  })

  socket.on('dead', function (id) {
    if (socket.playerId != id) return
    socket.player.life = 0
    socket.player.alive = false
    $('#life').css(
      'width',
      (socket.player.life / socket.player.maxLife) * 100 + '%'
    )
    $('#lifeText').html(socket.player.life + '/' + socket.player.maxLife)
    $('.controls').css('display', 'none')
  })

  socket.on('win', function () {
    setTimeout(function () {
      $('.controls').remove()
      $('.stats').remove()
      $('body').append(
        $('<p>').html('Félicitation vous avez gagné.'),
        $('<label for="roomNumber">').html('Numéro de salle'),
        $('<input id="roomNumber">'),
        $('<br>'),
        $('<br>'),
        $('<button>')
          .html('Connexion')
          .on('click', function () {
            window.location =
              socket.io.uri +
              socket.io.engine.hostname +
              ':' +
              socket.io.engine.port +
              '?roomId=' +
              $('#roomNumber').val()
          })
      )
    }, 3 * 1000)
  })
  socket.on('loose', function () {
    $('.controls').remove()
    $('.stats').remove()
    $('body').append(
      $('<label for="roomNumber">').html('Numéro de salle'),
      $('<input id="roomNumber">'),
      $('<br>'),
      $('<br>'),
      $('<button>')
        .html('Connexion')
        .on('click', function () {
          window.location =
            socket.io.uri +
            socket.io.engine.hostname +
            ':' +
            socket.io.engine.port +
            '?roomId=' +
            $('#roomNumber').val()
        })
    )
  })
  socket.on('disconnected', function (data) {
    $('#who option[value="' + data.username + '"]').remove()
  })
  socket.on('id', function (code) {
    if (socket.playerId != null) return
    socket.playerId = code
    socket.emit('id', socket.playerId)
  })
} else {
  $('.controller').remove()
  socket.count = 0
  socket.actions = 0
  socket.currId = randomInt(0, 3)
  socket.dead = 0
  socket.win = false
  socket.players = []

  socket.on('connect', function () {
    for (var i = 0; i < 4; i++) {
      $('#qrcode').html('')
    }
    socket.emit('notController')
  })

  socket.on('roomId', function (code) {
    socket.roomId = code
    $('.explain').html(
      'Pour jouer à ce jeu scannez un qr code ou tapez dans votre navigateur la ligne sous le qr code.<br>Vous avez votre manette en main. Vos statistiques sont affichées sur cette dernière.<br>Attaquez le boss, défendez ou soigner vos coéquipiers afin de battre le boss qui vous est présenté.<br>Bonne chance !<br>Numéro de salle : ' +
        socket.roomId
    )
    var tmp = new QRCode(
      document.getElementById('qrcode'),
      socket.io.uri +
        socket.io.engine.hostname +
        ':' +
        socket.io.engine.port +
        '?roomId=' +
        socket.roomId
    )
    $('#qrcode').append(
      $('<p>').html(
        socket.io.uri +
          socket.io.engine.hostname +
          ':' +
          socket.io.engine.port +
          '?roomId=' +
          socket.roomId
      )
    )
  })

  socket.on('healBoss', function (boss) {
    fight.boss.life = boss.life
    fight.heal.play()
    fight.heals[4].anims.play('heal')
  })

  socket.on('magicHurt', function (boss) {
    fight.bossMagic.play('darkness', true)
    fight.bossMagicSound.play()
    for (var i = 0; i < socket.players.length; i++) {
      fight.actors[socket.players[i]].sprite.anims.play(
        'magicHurt' + (socket.players[i] + 1),
        true
      )
    }
  })

  socket.on('id', function (code) {
    socket.players.push(code)
  })

  socket.on('connected', function (code) {
    socket.emit('sendId')
  })

  socket.on('isConnected', function (usernames) {
    socket.count++
    if (socket.count == 4) {
      launchGame()
    }
  })

  socket.on('heal', function (data) {
    fight.heal.play()
    fight.heals[data.id].anims.play('heal')
    fight.playerHeal[data.id].setText('+' + data.heal)
    setTimeout(function () {
      fight.playerHeal[data.id].setText('')
    }, 500)
  })

  socket.on('revive', function (data) {
    console.log(data)
    fight.actors[data.id].alive = true
    socket.dead--
  })

  socket.on('forcedLaunch', function () {
    console.log('launch game')
    launchGame()
  })

  socket.on('low', function (id) {
    console.log(id + ' is low')
    fight.actors[id].lowHP = true
    fight.actors[id].sprite.anims.play('lowHP' + (id + 1), true)
  })

  socket.on('unlow', function (id) {
    console.log(id + ' is low')
    fight.actors[id].lowHP = false
    fight.actors[id].sprite.anims.play('idle' + (id + 1), true)
    //fight.actors[id].sprite.anims.play('lowHP'+(id+1), true);
  })

  socket.on('wantToAttack', function (data) {
    console.log('wantToAttack :', data)
    if (fight.actors[parseInt(data.id)])
      fight.actors[parseInt(data.id)].sprite.anims.play(
        'chooseHit' + (parseInt(data.id) + 1),
        true
      )
    socket.actions++
    if (socket.actions + socket.dead == socket.count) {
      socket.emit('doTurn')
    }
  })

  socket.on('wantToHeal', function (data) {
    console.log('heal :', data)
    fight.actors[parseInt(data.id)].sprite.anims.play(
      'cast' + (parseInt(data.id) + 1),
      true
    )
    socket.actions++
    if (socket.actions + socket.dead == socket.count) {
      socket.emit('doTurn')
    }
  })

  socket.on('evade', function (data) {
    console.log('evade :', data)
    fight.actors[data.id].sprite.anims.play(
      'evade' + randomInt(1, 2) + (parseInt(data.id) + 1),
      true
    )
  })

  socket.on('wantToGuard', function (data) {
    console.log('want to guard :', fight.actors, data)
    console.log(fight.actors[data.id])
    fight.actors[data.id].sprite.anims.play(
      'guard' + (parseInt(data.id) + 1),
      true
    )
    socket.actions++
    if (socket.actions + socket.dead == socket.count) {
      socket.emit('doTurn')
    }
  })

  socket.on('player', function (boss) {
    socket.boss = boss
  })

  socket.on('dead', function (id) {
    fight.actors[id].sprite.anims.play('dead' + (parseInt(id) + 1), true)
    fight.actors[id].alive = false
    socket.dead++
    if (socket.dead >= socket.count) {
      socket.emit('loose')
    }
  })

  socket.on('damage', function (data) {
    fight.blow.play()
    fight.actors[data.id].chooseHit = false

    socket.boss = data.boss
    fight.actors[data.id].sprite.anims.play(
      'hit' + (parseInt(data.id) + 1),
      true
    )
    fight.doHit(randomInt(0, 1), 4)
    if (socket.boss.life <= 0) {
      socket.emit('win')
    } else {
      for (var i = 0; i < 10; i++) {
        setTimeout(function () {
          fight.boss.visible = !fight.boss.visible
        }, 100 * i)
      }
      fight.boss.visible = true
    }
  })

  socket.on('loose', function (data) {
    fight.ambiance.stop()
    fight.gameover.play()
    reboot(20)
  })

  socket.on('hurt', function (data) {
    fight.blow.play()
    fight.playerDammage[data.id].setText('-' + data.damage)
    fight.actors[data.id].sprite.anims.play(
      'physicHurt' + (parseInt(data.id) + 1),
      true
    )
    fight.doHit(randomInt(0, 1), data.id)
    setTimeout(function () {
      fight.playerDammage[data.id].setText('')
    }, 500)
  })

  socket.on('endTurn', function () {
    for (var i = 0; i < fight.actors.length; i++) {
      if (!fight.actors[i] || !fight.actors[i].alive) continue
      fight.actors[i].sprite.anims.play('idle' + (i + 1), true)
      if (fight.actors[i].lowHP)
        fight.actors[i].sprite.anims.play('lowHP' + (i + 1), true)
      if (socket.win)
        fight.actors[i].sprite.anims.play('victory' + (i + 1), true)
    }
    socket.actions = 0
    if (socket.boss.life > 0) fight.boss.visible = true
  })

  socket.on('win', function () {
    fight.ambiance.stop()
    fight.victory.play()
    socket.win = true
    fight.boss.visible = false
    for (var i = 0; i < fight.actors.length; i++) {
      if (!fight.actors[i] || !fight.actors[i].alive) continue
      fight.actors[i].sprite.anims.play('victory' + (i + 1), true)
    }
    reboot(6)
  })

  socket.on('needHeal', function (id) {
    fight.playerMessage[id].setText("J'ai besoin de soin")
    setTimeout(function () {
      fight.playerMessage[id].setText('')
    }, 500)
  })

  socket.on('disconnected', function (data) {
    socket.count--
    if (!fight.actors[data.id].alive) socket.dead--
    fight.actors[data.id].sprite.visible = false
    if (socket.count <= 0) socket.emit('loose')
  })
}

var reboot = function (time) {
  setTimeout(function () {
    location.reload()
  }, time * 1000)
}

$('#attack').on('click', function () {
  $('.controls').css('display', 'none')
  socket.emit('attack')
})

$('#guard').on('click', function () {
  $('.controls').css('display', 'none')
  socket.emit('guard')
})

$('#heal').on('click', function () {
  $('.controls').css('display', 'none')
  socket.emit('heal', $('input[name=who]:checked').val())
})

$('#send').on('click', function () {
  if (!mobilecheck()) return
  socket.emit('controller', $('#username').val())
  $('#form').remove()
})

$('#force').on('click', function () {
  console.log('plop', socket)
  socket.emit('forcedLaunch')
})

$('#needHeal').on('click', function () {
  socket.emit('needHeal')
})

function randomInt(low, high) {
  return Phaser.Math.RND.integerInRange(low, high)
}

function $_GET(param) {
  var vars = {}
  window.location.href.replace(location.hash, '').replace(
    /[?&]+([^=&]+)=?([^&]*)?/gi, // regexp
    function (m, key, value) {
      // callback
      vars[key] = value !== undefined ? value : ''
    }
  )

  if (param) {
    return vars[param] ? vars[param] : null
  }
  return vars
}
